import XCTest

import wbTests

var tests = [XCTestCaseEntry]()
tests += wbTests.allTests()
XCTMain(tests)
