// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
   name: "wb",
   products: [
     .executable(name: "wb", targets: ["wb"]),
   ],
   dependencies: [
     .package(url: "https://github.com/swift-server/swift-aws-lambda-runtime.git", .upToNextMajor(from:"0.3.0")),
     .package(url:"git@bitbucket.org:ssweeney-apg/wb-library.git",.branch("master")),
   ],
   targets: [
     .target(
       name: "wb",
       dependencies: [
         .product(name: "AWSLambdaRuntime", package: "swift-aws-lambda-runtime"),
         .product(name: "wb-library", package: "wb-library"),
       ]
     )
   ]
 )
