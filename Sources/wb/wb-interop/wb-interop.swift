//
//  wb-interop.swift
//  wb-framework
//
//  Created by Victor Graham on 10/7/20.
//  Copyright © 2020 Aircraft Performance Group. All rights reserved.
//

import Foundation
import WBLibrary

//Class used to provide a Swift interface to the weight and balance C library
public class WBInterop
{
    //Standard fuel mode - Calculates graph data given a loaded fuel weight, taxiFuel weight, and an enroute fuel weight.
    public static func calculateGraphData(tail swiftTail: WBStructs.Tail, loadedFuelWeight: Double, taxiFuelWeight: Double, enrouteFuelWeight: Double) -> WBStructs.GraphData
    {
        
        return calculateGraphData(tail: swiftTail,
                                  loadedFuelWeight: loadedFuelWeight,
                                  taxiFuelWeight: taxiFuelWeight,
                                  enrouteFuelWeight: enrouteFuelWeight,
                                  customLoadWeights: [Double](),
                                  customBurnProfile: WBStructs.CustomBurnProfile())
    }
    
    //Custom load fuel mode - Calculates graph data given an array of custom load weights, taxiFuel weight, and an enroute fuel weight.
    public static func calculateGraphData(tail swiftTail: WBStructs.Tail, customLoadWeights: [Double], taxiFuelWeight: Double, enrouteFuelWeight: Double) -> WBStructs.GraphData
    {
        
        return calculateGraphData(tail: swiftTail,
                                  loadedFuelWeight: customLoadWeights.reduce(0, +),
                                  taxiFuelWeight: taxiFuelWeight,
                                  enrouteFuelWeight: enrouteFuelWeight,
                                  customLoadWeights: customLoadWeights,
                                  customBurnProfile: WBStructs.CustomBurnProfile())
    }

    //Custom load and burn fuel mode - Calculates graph data given an array of custom load weights, custom burn profile, and an enroute fuel weight.
    public static func calculateGraphData(tail swiftTail: WBStructs.Tail, customLoadWeights: [Double], customBurnProfile: WBStructs.CustomBurnProfile, enrouteFuelWeight: Double) -> WBStructs.GraphData
    {
        
        return calculateGraphData(tail: swiftTail,
                                  loadedFuelWeight: customLoadWeights.reduce(0, +),
                                  taxiFuelWeight: customBurnProfile.taxiFuelWeight,
                                  enrouteFuelWeight: enrouteFuelWeight,
                                  customLoadWeights: customLoadWeights,
                                  customBurnProfile: customBurnProfile)
    }


    //Called by the other three public calculateGraphData function overloads.
    //This one calls CalculateGraphData in graph_data.c
    private static func calculateGraphData(tail swiftTail: WBStructs.Tail, loadedFuelWeight: Double, taxiFuelWeight: Double, enrouteFuelWeight: Double, customLoadWeights: [Double], customBurnProfile: WBStructs.CustomBurnProfile) -> WBStructs.GraphData
    {
        var cTail = WBMarshal.marshalTail(swiftTail: swiftTail);
        
        var graphData = createGraphData(maxFuelPointCount: cTail.max_fuel_point_count)
        
        let customLoadWeightsCArray = WBMarshal.marshalDoubleArray(swiftDoubles: customLoadWeights)
        var cCustomBurnProfile = WBMarshal.marshalCustomBurnProfile(swiftCustomBurnProfile: customBurnProfile)
    
        defer
        {
            WBMarshal.freeTail(cTail: cTail)
            freeGraphData(graphData: graphData)
            
            customLoadWeightsCArray.deallocate()
            WBMarshal.freeCustomBurnProfile(cCustomBurnProfile: cCustomBurnProfile)
        }

        //Call CalculateGraphData in graph_data.c
        let errorCode = CalculateGraphData(&cTail, loadedFuelWeight, taxiFuelWeight, enrouteFuelWeight, customLoadWeightsCArray, Int32(customLoadWeights.count), &cCustomBurnProfile, &graphData)
            
        var swiftGraphData = WBUnmarshal.unmarshalGraphData(cGraphData: graphData);
        swiftGraphData.error = getError(code: errorCode)
        
        return swiftGraphData
    }
    
    //Creates an Error Object given an ErrorCodes enum from the c library
    static func getError(code: ErrorCodes) -> WBStructs.Error
    {
        if let message = wbErrors[Int(code.rawValue)]
        {
            return WBStructs.Error(code: Int(code.rawValue),
                                   message: "Error: " + message)
        } else
        {
            return WBStructs.Error(code: Int(code.rawValue),
                                   message: "Unknown Error")
        }
    }
    
    //Creates a c graphData struct and allocates memory for the various fuel burn vectors.  Memory must be deallocated.
    //Function freeGraphData is designed to deallocate the memory that is allocated here.
    static func createGraphData (maxFuelPointCount: Int) -> GraphData
    {
        var graphData = GraphData()
        
        graphData.fuelVectors.enroute.minDensity = UnsafeMutablePointer<Mass>.allocate(capacity: Int(maxFuelPointCount))
        graphData.fuelVectors.enroute.standardDensity = UnsafeMutablePointer<Mass>.allocate(capacity: Int(maxFuelPointCount))
        graphData.fuelVectors.enroute.maxDensity = UnsafeMutablePointer<Mass>.allocate(capacity: Int(maxFuelPointCount))

        graphData.fuelVectors.ballast.minDensity = UnsafeMutablePointer<Mass>.allocate(capacity: Int(maxFuelPointCount))
        graphData.fuelVectors.ballast.standardDensity = UnsafeMutablePointer<Mass>.allocate(capacity: Int(maxFuelPointCount))
        graphData.fuelVectors.ballast.maxDensity = UnsafeMutablePointer<Mass>.allocate(capacity: Int(maxFuelPointCount))
        
        return graphData
    }
    
    //Frees memory dynamically allocated to member of a c GraphData struct.
    //Intended to work with createGraphData.
    static func freeGraphData (graphData: GraphData) -> ()
    {
        graphData.fuelVectors.enroute.minDensity.deallocate()
        graphData.fuelVectors.enroute.standardDensity.deallocate()
        graphData.fuelVectors.enroute.maxDensity.deallocate()

        graphData.fuelVectors.ballast.minDensity.deallocate()
        graphData.fuelVectors.ballast.standardDensity.deallocate()
        graphData.fuelVectors.ballast.maxDensity.deallocate()
    }
}
