//
//  WB-Enums.swift
//  wb-framework
//
//  Created by Victor Graham on 10/5/20.
//  Copyright © 2020 Aircraft Performance Group. All rights reserved.
//

import Foundation

//Namespace for WB enums
public enum WBEnums {
    //enum for limits that may be violated
    enum Limits: Int {
        case none
        case rampWeight
        case takeoffWeight
        case landingWeight
        case zeroFuelWeight
        case minimumWeight
        case minimumCG
        case maximumCG
    }


    //enum for burn conditions
    public enum BurnConditions: Int, Codable {
        case burnConditionNone = 2
        case tankIsEmpty = 4
        case percentRemaining = 6
        case untilEmpty = 8
        #warning("public extension BurnCondition there is no case .untilEmpty")
    }
    
    
    //enum for burn types
    public enum BurnTypes: Int, Codable {
        case burnFrom = 2
        case burnDownToRemaining = 4
        case transferFuel = 6
        case hardCodeBurn = 8
        case ratioBurn = 10
    }
    
    //enum for hard-code burn rule types
    public enum HardCodeBurnTypes: Int, Codable {
        case CL604
        case CL604SB
        case CL605
        case B727200
        case None
        
        #warning("Converting from ProfileBurnRule hardCodeBurnName: String?")
        public init(_ string: String) {
            switch string {
            case "CL604":
                self = .CL604
            case "CL604SB":
                self = .CL604SB
            case "CL605":
                self = .CL605
            case "B727200":
                self = .B727200
            default:
                self = .None
            }
        }
    }

    //TODO:  Rename enum values using normal conventions in rewite
    //Keep in legacy code because raw string values need to match

    //enum for load conditions
    public enum LoadConditions: Int, Codable {
        case loadConditionNone = 0   //Fill tank with remaining user entered fuel until tank is full.
        case mustFill = 3       //Must fill this tank full with user entered fuel.  If this can not occur, this rule is skipped.
        case continueLoad = 5   //Continue loading on this rule until user entered fuel is exausted.
        case fillWithRatio = 6  //Fill specified fuel tanks using a ratio. Must use UntilTankFull/Gallons tag with fueltankid specified.
        case untilFull = 7      //Continue loading until tank is full.
    }

    /*
    //Enum for weight types.  Copied from legacy code.
    enum SelectedWeightType: String {
        case Default
        case PAX
        case Crew
    }
 */

    //Enum for which envelopes are affected by a movement curtailment.  Copied from legacy code.
    public enum AffectedEnvelope: String, Codable {
        case Takeoff
        case Inflight
        case Landing
        case ZeroFuel
    }

    //Enum for defining the role of a curtailment movement.  Copied from legacy code.
    public enum MovementRole: String, Codable {
        case APGMovementRoleNone
        case APGMovementRolePAX
        case APGMovementRoleWorkingCabinCrewMember
        case APGMovementRoleAll
        case APGMovementRoleCart
    }

    //Enum for defining the type of movement curtailment.  Copied from legacy code.
    public enum MovementType: String, Codable {
        case APGMovementTypeConstant
        case APGMovementTypeDynamic
        case APGMovementTypeUnknown
    }

    //Enum for crew movement mode.  Copied from legacy code.
    public enum CrewMovementMode: Int, Codable {
        case all = 0        //NoRestrictions
        case offset = 2     //PaxForwardRequired
        case none = 4       //NoFlightCrewToLav
    }
}
