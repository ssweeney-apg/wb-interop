//
//  wb-constants.swift
//  wb-framework
//
//  Created by Victor Graham on 10/20/20.
//  Copyright © 2020 Aircraft Performance Group. All rights reserved.
//

import Foundation


//Axes constants
public enum Axis: Int {
    case x = 0
    case y = 1
    case z = 2
}

//Default ID number, useful to prevent bugs due a default ID that is valid
public let defaultID = Int.min

//Error codes with associated messages
let wbErrors = [0: "None",
                1000: "Critical Failure",
                1001: "Tank ID Not Found",
                1002: "CG Extrapolate Low",
                1003: "CG Extrapolate High",
                1004: "Invalid Design Envelope Definition",
                1005: "Invalid Fixed Curtailment Envelope Definition",
                1006: "Invalid Taxi Burn Tank Index",
                1100: "Insufficient Fuel Capacity",
                1101: "Insufficient Fuel",
                1102: "Insufficient Burn Rules",
                1200: "Load Rule Not Set",
                1201: "Load Rule Not Supported",
                1202: "Insufficient Load Rules",
                1300: "Burn Rule Not Set",
                1301: "Burn Rule Not Supported",
                1302: "Burn Rule Condition Not Met",
                1400: "Unable to Find Ballast",
                1500: "Cabin Item Not Found",
                1501: "Invalid Cabin Item Sub Type",
                1600: "Curtailment Not Found"]
