//
//  WB-Structs.swift
//  wb-framework
//
//  Created by Victor Graham on 10/5/20.
//  Copyright © 2020 Aircraft Performance Group. All rights reserved.
//

import Foundation

//Namsespace for WB structs
public struct WBStructs
{
    //Struct to hold mean aerodynamic cord data
    public struct MAC: Codable
    {
        var length: Double = 0
        var leadingEdge: Double = 0
        
        public init(){}
        
        public init(length: Double, leadingEdge: Double) {
            self.length = length
            self.leadingEdge = leadingEdge
        }
        
        //Methods
        public func stationToPerCentMAC(station: Double) -> Double
        {
            return (station - self.leadingEdge) / self.length * 100
        }
        
        public func perCentMACToStation(perCentMAC: Double) -> Double
        {
            return perCentMAC / 100 * self.length + self.leadingEdge
        }
    }
    
    
    //Struct to hold a weight and an array of moments and provide related functions
    public struct Mass: Codable {
        //Properties
        //Each element of the moment array represents a geometric dimension (i.e. x, y, z)
        //Default is 3 dimensions
        public var weight: Double = 0
        public var moment: [Double] = Array(repeating: 0, count: 3)
        
        public var cg: [Double] {
            get {
                //Calculate CGs
                //Calling function is responsible for handling zero-weight condition
                return moment.map{$0 / self.weight};
            }
            set {
                self.moment = newValue.map{$0 * self.weight}
            }
        }
        
        public init (weight: Double, moment: [Double]) {
            self.weight = weight
            
            var moment = moment
            while moment.count < 3 {
                moment.append(0)
            }
            self.moment = moment
        }
        
        public init (weight: Double, cg: [Double]) {
            self.weight = weight
            
            var cg = cg
            while cg.count < 3 {
                cg.append(0)
            }
            self.cg = cg
        }
        
        init() {}
        
        //Methods
        
        /*
         *********************************************************************************************************
         Note about the number of dimensions for moment:
         
         The results from the + and - overloads return a number of moment elements equal to the lower number of
         moment elements of the two arguments passed into the functions.  If one Mass has a 2D moment and the
         other has a 3D moment, the Mass returned will have a 2D moment.
         
         The C library Mass struct supports a maximum of 3 dimensions.
         ********************************************************************************************************
         */
        
        //Sums elements of a Mass array
        //Maximum moment dimensions supported is 3.
        static func sumArray(masses: [Mass]) -> Mass
        {
            var sum = Mass(weight: 0, moment: [0, 0, 0]);
            
            for mass: Mass in masses
            {
                sum = sum + mass;
            }
            
            return sum;
        }
        
        
        //Function overloads
        
        //Adding/subtracting array elements described here:
        //https://stackoverflow.com/questions/41453942/add-elements-of-two-arrays-in-swift-without-appending-together

        //Add
        static func + (left: Mass, right: Mass) -> Mass
        {
            let newMoments = zip(left.moment, right.moment).map(+);
            
            return Mass(weight: left.weight + right.weight, moment: newMoments);
        }

        //Subtract
        static func - (left: Mass, right: Mass) -> Mass
        {
            let newMoments = zip(left.moment, right.moment).map(-);
            
            return Mass(weight: left.weight - right.weight, moment: newMoments);
        }
    }
    
    
    //Struct to hold a volume and an array of cg's
    //Each element of the cg array represents a geometric dimension (i.e. x, y, z)
    //Default is 3 dimensions
    //The C library Volume struct supports a maximum of 3 dimensions.
    public struct Volume: Codable {
        public var volume: Double = 0
        public var cg: [Double] = Array(repeating: 0, count: 3)
        
        public init (volume: Double, cg: [Double]) {
            self.volume = volume
            
            var cg = cg
            while cg.count < 3 {
                cg.append(0)
            }
            self.cg = cg
        }
        

        public init() {}
    }
    
    
    //Struct to hold fluid tank data
    public struct TankData: Codable {
        //It is the instancing function's responsibility to ensure the volumePoints array is valid data sorted in ascending order by volume
        public var ID: Int = 0
        public var density: Double = 0.0
        public var volumePoints = [Volume]()

        var capacity: Double {
            get {
                volumePoints[volumePoints.count - 1].volume
            }
        }
        
        public init(ID: Int, density: Double, volumePoints: [Volume]) {
            self.ID = ID
            self.density = density
            self.volumePoints = volumePoints
        }
        

        public init() {}
    }
    
    
    //Struct to hold weight limits
    public struct WeightLimits: Codable {
        public var maximumRampWeight: Double = 0
        public var maximumTakeoffWeight: Double = 0
        public var maximumLandingWeight: Double = 0
        public var maximumZeroFuelWeight: Double = 0
        public var minimumWeight: Double = 0
        
        public init() {}
    }
    
    
    //Struct to hold envelope data
    public struct Envelope: Codable {
        public var minimumWeight: Double = 0
        public var maximumWeight: Double = 0
        public var minimumCGLimitPoints = [Mass]();
        public var maximumCGLimitPoints = [Mass]();
        
        public init() {}
        
        public init (minimumWeight: Double, maximumWeight: Double, minimumCGLimitPoints: [Mass], maximumCGLimitPoints: [Mass]) {
            self.minimumWeight = minimumWeight
            self.maximumWeight = maximumWeight
            self.minimumCGLimitPoints = minimumCGLimitPoints
            self.maximumCGLimitPoints = maximumCGLimitPoints
        }
    }
    
    //Struct to hold desgin envelopes
    public struct StandardEnvelopes: Codable {
        public var inflight = Envelope()
        public var takeoff = Envelope()
        public var landing = Envelope()
        public var zeroFuel = Envelope()
        
        public init() {}
    }
    
    //Struct to hold various aircraft envelopes
    public struct TailEnvelopes: Codable {
        public var design = StandardEnvelopes()
        public var fixedCurtailment = Envelope()
        
        public init() {}
    }

    //Struct to hold fuel ratio data
    public struct FuelRatio: Codable {
        var fuelTankID: Int = 0
        var ratioVolume: Double = 0
        
        public init(fuelTankID: Int,
                    ratioVolume: Double){
            self.fuelTankID = fuelTankID
            self.ratioVolume = ratioVolume
        }
        
        public init() {}
    }
    
    
    //Struct for burn rule data
    public struct BurnRule: Codable {
        public var fuelTankID: Int = 0    //ID of Fuel Tank to be burned
        public var destinationFuelTankID: Int = 0 //ID of Fuel Tank to which fuel is to be transferred
        public var weight: Double = 0 //If BurnType is BurnFrom then Weight to Burn
                                //If BurnType is BurnDownToRemainder then Remainder Weight
                                //If BurnType is TransferFuel then Transfer Weight
                                //If BurnType is HardCodeBurn then fuel to burn based on HardCodeBurnName (not supported)
        public var sequence: Int = 0  //Rule Sequence
        public var type: WBEnums.BurnTypes = .burnDownToRemaining  //Type of Burn
        public var condition: WBEnums.BurnConditions = .burnConditionNone  //Burn From Condition
        public var conditionalFuelTankID = 0  //Conditional Tank ID
        public var conditionValue: Double = 0
        public var fuelRatios = [FuelRatio]()
        public var hardCodeBurnType: WBEnums.HardCodeBurnTypes = .None   //Name of Hard Code Burn Profile
        
        public init(fuelTankID: Int,
                    destinationFuelTankID: Int,
                    weight: Double,
                    sequence: Int,
                    type: WBEnums.BurnTypes,
                    condition: WBEnums.BurnConditions,
                    conditionalFuelTankID: Int,
                    conditionValue: Double,
                    fuelRatios: [FuelRatio],
                    hardCodeBurnType: WBEnums.HardCodeBurnTypes)
        {
            self.fuelTankID = fuelTankID
            self.destinationFuelTankID = destinationFuelTankID
            self.weight = weight
            self.sequence = sequence
            self.type = type
            self.condition = condition
            self.conditionalFuelTankID = conditionalFuelTankID
            self.conditionValue = conditionValue
            self.fuelRatios = fuelRatios
            self.hardCodeBurnType = hardCodeBurnType
        }
        
        public init() {}
    }
    
    //Struct for fuel load data
    public struct FuelLoad: Codable {
        public var loadRules = [LoadRule]()
        public var maxFuelLoadGallons = 0.0
        public var minFuelLoadGallons = 0.0
        public var weightHigh = 0.0
        public var weightLow = 0.0
        public init(
            loadRules: [LoadRule],
            maxFuelLoadGallons: Double,
            minFuelLoadGallons: Double,
            weightHigh: Double,
            weightLow: Double
        ) {
            self.loadRules = loadRules
            self.maxFuelLoadGallons = maxFuelLoadGallons
            self.minFuelLoadGallons = minFuelLoadGallons
            self.weightHigh = weightHigh
            self.weightLow = weightLow
        }
    }

    //Struct for load rule data
    public struct LoadRule: Codable {
        public var sequence: Int = 0
        public var fuelTankID: Int = defaultID
        public var loadVolume: Double = 0.0
        public var fuelRatios = [FuelRatio]()
        public var untilFullTankID: Int = defaultID
        public var condition: WBEnums.LoadConditions = .loadConditionNone
        
        public init(sequence: Int,
                    fuelTankID: Int,
                    loadVolume: Double,
                    fuelRatios: [FuelRatio],
                    untilFullTankID: Int,
                    condition: WBEnums.LoadConditions)
        {
            self.sequence = sequence
            self.fuelTankID = fuelTankID
            self.loadVolume = loadVolume
            self.fuelRatios = fuelRatios
            self.untilFullTankID = untilFullTankID
            self.condition = condition
        }
        
        public init() {}
    }

    //Struct for random loading schedule data for a passenger zone.  Copied from legacy code.
    public struct RandomLoadingSchedule: Codable {
        public var cabinItemId = defaultID
        public var cgShiftAftLoading = 0.0
        public var cgShiftFwdLoading = 0.0
        //public var floorplanId = defaultID
        public var paxIndex = 0
        public var randomLoadingScheduleId = defaultID
        
        public init () {}
    }

    //Copied from legacy code
    public struct WeightForCargo: Codable {
        var count = 0
        var name = ""
        var totalWeight = 0.0
        var weight = 0.0
    }

    //Struct for cargo details.  Copied from legacy code.
    public class CargoDetails: Codable {
        var cargoWeights = [WeightForCargo]()
        var otherCount = 0
        var otherTotalWeight = 0.0
        var otherWeight = 0.0
    }

    //Struct for cabin item data.  Copied from legacy code.
    public struct CabinItem: Codable {
        public var arm = 0.0
        public var cabinItemID = defaultID
        public var centroid = 0.0
        public var filled = false
        public var maxCount = 0
        public var maxWeight = 0.0
        public var name = ""
        public var randomLoadingSchedules = [RandomLoadingSchedule]()
        public var referenceCabinItemID = defaultID
        public var seatCount = 0
        public var subType = ""
        public var weight = 0.0
        //var weightType = WBEnums.SelectedWeightType.
        
        public init(arm: Double,
                    cabinItemID: Int,
                    centroid: Double,
                    filled: Bool,
                    maxCount: Int,
                    maxWeight: Double,
                    name: String,
                    randomLoadingSchedules: [RandomLoadingSchedule],
                    referenceCabinItemID: Int,
                    seatCount: Int,
                    subType: String,
                    weight: Double)
        {
            self.arm = arm
            self.cabinItemID = cabinItemID
            self.centroid = centroid
            self.filled = filled
            self.maxCount = maxCount
            self.maxWeight = maxWeight
            self.name = name
            self.randomLoadingSchedules = randomLoadingSchedules
            self.referenceCabinItemID = referenceCabinItemID
            self.seatCount = seatCount
            self.subType = subType
            self.weight = weight
        }
        public init() {}
    }

    //Struct to hold curtailment settings.
    public struct CurtailmentSettings: Codable {
        public var useCurtailmentEnvelope = false
        public var hasRealtimeCurtailment = false
        public var useRealtimeCurtailment = false
        public var crewMovementMode = WBEnums.CrewMovementMode.none
        
        public init() {}
    }

    //Struct for curtailment movements.  Copied from legacy code
    public struct Movement: Codable {
        public var actions = [MovementAction]()
        public var affectedEnvelopes = [WBEnums.AffectedEnvelope]()
        public var constantMovementValue = 0.0
        public var movementId = defaultID
        public var role = WBEnums.MovementRole.APGMovementRoleNone
        public var standardWeightForMovement = 0.0
        public var type = WBEnums.MovementType.APGMovementTypeUnknown
        
        public init() {}
    }

    //Struct for curtailment movement actions.  Copied from legacy code
    public struct MovementAction: Codable {
        public var endingCabinItemId = defaultID
        public var movementActionId = defaultID
        public var movementId = defaultID
        public var offsetUseOnly = false
        public var requireWeightInEndingCabinItem = false
        public var startingCabinItemId = defaultID
        
        public init () {}
    }

    //Struct for real-time curtailment data
    public struct Curtailments: Codable {
        public var settings = CurtailmentSettings()
        public var movements = [Movement]()
        public var minimumFuelDensity = 6.3
        public var maximumFuelDensity = 7.1
        
        public init () {}
    }

    //Struct for tail data
    public struct Tail: Codable {
        //C Tail structs support a number of envelopes equal to the kEnvelopeTypes constant from the C library.
        //The C library uses the EnvelopeTypes enum as array indices to accesses members of the design envelopes array.
        //wb-constants includes a set of constants equivalent to the EnvelopeTypes enum intended to be used as array indices.
        public var basicEmpty = Mass()
        public var cabinItems = [CabinItem]()
        public var mac = MAC();
        public var weightLimits = WeightLimits();
        public var envelopes = TailEnvelopes()
        public var fuelTankData = [TankData]();
        public var taxiBurnTankIndex = -1
        public var fuelDensity = 0.0;
        public var fuelLoads = [FuelLoad]();
        public var burnRules = [BurnRule]();
        public var curtailments = Curtailments()
        public var zeroFuelEnvelopeOnly = false

        public var fuelVolumeCapacity: Double {
            get {
                var totalCapacity = 0.0

                for tank in self.fuelTankData {
                    totalCapacity += tank.capacity
                }

                return totalCapacity
            }
        }

        public var fuelWeightCapacity: Double {
            get {
                self.fuelVolumeCapacity * self.fuelDensity
            }
        }
        
        public init() {}
    }

    //Struct to hold fuel weights
    public struct FuelWeights: Codable
    {
        public var enroute = Mass();   //Weight and cg for enroute fuel
        public var landing = Mass();   //Weight and cg for fuel on board at landing
        public var ballast = Mass();   //Weight and cg for ballast fuel
        public var all = Mass();       //Weight and cg for all loaded fuel
    }

    //Struct to hold fuel vectors of min, standard, and max fuel densitites
    public struct FuelVector: Codable
    {
        public var minDensity = [Mass]()
        public var standardDensity = [Mass]()
        public var maxDensity = [Mass]()
    }
    
    //Struct to hold various parts of the fuel vector
    public struct FuelVectors: Codable
    {
        //TODO: Implement taxi vector in new system and include it in all
        public var enroute = FuelVector()  //Vector for enroute fuel
        public var ballast = FuelVector()  //Vector for ballast fuel
    }
    
    //Struct to hold error data
    public struct Error: Codable
    {
        var code = 0;
        var message = wbErrors[0];
    }

    //Struct to hold min and max CG limits
    public struct CGLimits: Codable {
        public var minimum = Mass()
        public var maximum = Mass()
    }

    //Struct to hold critical weight calulations
    public struct CriticalWeights: Codable {
        public var taxi = Mass()
        public var takeoff = Mass()
        public var landing = Mass()
        public var zeroFuel = Mass()
        public var basicOperating = Mass()
    }

    //Struct to hold CG limits at critical weights
    public struct CriticalCGLimits: Codable {
        public var zeroFuel = CGLimits()
        public var taxi = CGLimits()
        public var takeoff = CGLimits()
        public var landing = CGLimits()
    }

    //Struct to hold graph data
    public struct GraphData: Codable {
        public var criticalWeights = CriticalWeights()
        public var criticalCGLimits = CriticalCGLimits()
        public var fuelWeights = FuelWeights()
        public var fuelVectors = FuelVectors()
        public var fuelBurnIsOutOfEnvelope = false
        public var curtailedEnvelopes = [Envelope]()
        public var error = Error()
    }
    
    //struct to hold a weight and tank ID.  Useful for custom burn definitions
    public struct BurnWeight: Codable
    {
        public var fuelTankID = defaultID
        public var weight = 0.0
        
        public init() {}
        
        public init(fuelTankID: Int, weight: Double) {
            self.fuelTankID = fuelTankID
            self.weight = weight
        }
    }
    
    //Struct to hold a custom burn profile definition
    public struct CustomBurnProfile: Codable
    {
        var taxiFuelWeight = 0.0
        var taxiBurnTankIndex = -1
        var burnWeights = [BurnWeight]()
        
        public init(taxiFuelWeight: Double,
                    taxiBurnTankIndex: Int,
                    burnWeights: [BurnWeight])
        {
            self.taxiFuelWeight = taxiFuelWeight
            self.taxiBurnTankIndex = taxiBurnTankIndex
            self.burnWeights = burnWeights
        }
        
        public init() {}
    }
}
