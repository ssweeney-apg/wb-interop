import WBLibrary
import AWSLambdaRuntime
import Foundation

// Request, uses Codable for transparent JSON encoding
private struct Request: Codable {
    let tail: WBStructs.Tail
    let fuelWeights: fuelWeights
}

// Response, uses Codable for transparent JSON encoding
private struct Response: Codable {
    let graphData: WBStructs.GraphData
}

private struct fuelWeights: Codable {
    let loadedFuelWeight: Double?
    let customLoadWeights: [Double]?
    let customBurnProfile: WBStructs.CustomBurnProfile?
    let taxiFuelWeight: Double
    let enrouteFuelWeight: Double
    let fuelBurnType: FuelBurnType
}

enum FuelBurnType: String, Codable {
    case standardBurn
    case customLoad
    case customLoadBurn
}

Lambda.run { (context, request: Request, callback: @escaping (Result<Response, Error>) -> Void) in
    
    //TODO Handle possible errors
    var graphData: WBStructs.GraphData = WBStructs.GraphData()
    
    switch request.fuelWeights.fuelBurnType {
    case .standardBurn:
        graphData = WBInterop.calculateGraphData(tail: request.tail, loadedFuelWeight: request.fuelWeights.loadedFuelWeight!, taxiFuelWeight: request.fuelWeights.taxiFuelWeight, enrouteFuelWeight: request.fuelWeights.enrouteFuelWeight)
    case .customLoad:
        graphData = WBInterop.calculateGraphData(tail: request.tail, customLoadWeights: request.fuelWeights.customLoadWeights!, taxiFuelWeight: request.fuelWeights.taxiFuelWeight, enrouteFuelWeight: request.fuelWeights.enrouteFuelWeight)
    case .customLoadBurn:
        graphData = WBInterop.calculateGraphData(tail: request.tail, customLoadWeights: request.fuelWeights.customLoadWeights!, customBurnProfile: request.fuelWeights.customBurnProfile!, enrouteFuelWeight: request.fuelWeights.enrouteFuelWeight)
    }
    
    callback(.success(Response(graphData: graphData)))
    
}
