//
//  wb-unmarshall.swift
//  wb-framework
//
//  Created by Victor Graham on 10/8/20.
//  Copyright © 2020 Aircraft Performance Group. All rights reserved.
//

import Foundation
import WBLibrary

//Class used to convert weight and balance C library data structures to Swift weight and balance data structures.
class WBUnmarshal
{
    //Converts an array of C Mass structs to Swift Mass structs
    static func unmarshalMasses(cMasses: UnsafeMutablePointer<Mass>, elementCount: Int) -> [WBStructs.Mass]
    {
        var swiftMasses = [WBStructs.Mass]();
        
        for i in 0..<elementCount
        {
            swiftMasses.append(unmarshalMass(cMass: cMasses[i]));
        }
        
        return swiftMasses;
    }
    
    //Converts a C Mass struct to a Swift Mass struct
    static func unmarshalMass(cMass: Mass) -> WBStructs.Mass
    {
        return WBStructs.Mass(weight: cMass.weight,
                              moment: [cMass.moment.0,
                                       cMass.moment.1,
                                       cMass.moment.2])
    }

    //Converts a C CriticalWeights struct to a Swift CriticalWeights struct
    static func unmarshalCriticalWeights(cCriticalWeights: CriticalWeights) -> WBStructs.CriticalWeights
    {
        return WBStructs.CriticalWeights(taxi: unmarshalMass(cMass: cCriticalWeights.taxi),
                                         takeoff: unmarshalMass(cMass: cCriticalWeights.takeoff),
                                         landing: unmarshalMass(cMass: cCriticalWeights.landing),
                                         zeroFuel: unmarshalMass(cMass: cCriticalWeights.zeroFuel),
                                         basicOperating: unmarshalMass(cMass: cCriticalWeights.basicOperating))
    }

    //Converts a C CGLimits struct to a Swift Mass struct
    static func unmarshalCGLimits(cCGLimits: MomentLimits, weight: Double) -> WBStructs.CGLimits
    {
        return WBStructs.CGLimits(minimum: WBStructs.Mass(weight: weight,
                                                          moment: [cCGLimits.minimum.0, cCGLimits.minimum.1, cCGLimits.minimum.2]),
                                  maximum: WBStructs.Mass(weight: weight,
                                                          moment: [cCGLimits.maximum.0, cCGLimits.maximum.1, cCGLimits.maximum.2]))
        
        //(minimum: [cCGLimits.minimum.0, cCGLimits.minimum.1, cCGLimits.minimum.2],
        //maximum: [cCGLimits.maximum.0, cCGLimits.maximum.1, cCGLimits.maximum.2])
    }
    
    //Converts a C CriticalCGLimits struct to a Swift CriticalCGLimits struct
    static func unmarshalCriticalCGLimits(cCriticalCGLimits: CriticalMomentLimits, cCriticalWeights: CriticalWeights) -> WBStructs.CriticalCGLimits
    {
        return WBStructs.CriticalCGLimits(zeroFuel: unmarshalCGLimits(cCGLimits: cCriticalCGLimits.zeroFuel, weight: cCriticalWeights.zeroFuel.weight),
                                          taxi: unmarshalCGLimits(cCGLimits: cCriticalCGLimits.taxi, weight: cCriticalWeights.taxi.weight),
                                          takeoff: unmarshalCGLimits(cCGLimits: cCriticalCGLimits.takeoff, weight: cCriticalWeights.takeoff.weight),
                                          landing: unmarshalCGLimits(cCGLimits: cCriticalCGLimits.landing, weight: cCriticalWeights.landing.weight))
    }
    
    //Converts a C FuelWeights struct to a Swift FuelWeights struct
    static func unmarshalFuelWeights(cFuelWeights: FuelWeights) -> WBStructs.FuelWeights
    {
        return WBStructs.FuelWeights(enroute: unmarshalMass(cMass: cFuelWeights.enroute),
                                     landing: unmarshalMass(cMass: cFuelWeights.landing),
                                     ballast: unmarshalMass(cMass: cFuelWeights.ballast),
                                     all: unmarshalMass(cMass: cFuelWeights.all))
    }

    //Converts a C FuelVector struct to a Swift FuelVector struct
    static func unmarshalFuelVector(cFuelVector: FuelVector) -> WBStructs.FuelVector
    {
        return WBStructs.FuelVector(minDensity: unmarshalMasses(cMasses: cFuelVector.minDensity, elementCount: Int(cFuelVector.minDensityPointCount)),
                                    standardDensity: unmarshalMasses(cMasses: cFuelVector.standardDensity, elementCount: Int(cFuelVector.standardDensityPointCount)),
                                    maxDensity: unmarshalMasses(cMasses: cFuelVector.maxDensity, elementCount: Int(cFuelVector.maxDensityPointCount)))
    }
    
    //Converts a C FuelVectors struct to a Swift FuelVectors struct
    static func unmarshalFuelVectors(cFuelVectors: FuelVectors) -> WBStructs.FuelVectors
    {
        return WBStructs.FuelVectors(enroute: unmarshalFuelVector(cFuelVector: cFuelVectors.enroute),
                                     ballast: unmarshalFuelVector(cFuelVector: cFuelVectors.ballast))
    }
    
    //Converts a C GraphData struct to a Swift GraphData struct
    static func unmarshalGraphData(cGraphData: GraphData) -> WBStructs.GraphData
    {
        return WBStructs.GraphData(criticalWeights: unmarshalCriticalWeights(cCriticalWeights: cGraphData.criticalWeights),
                                   criticalCGLimits: unmarshalCriticalCGLimits(cCriticalCGLimits: cGraphData.criticalMomentLimits, cCriticalWeights: cGraphData.criticalWeights),
                                   fuelWeights: unmarshalFuelWeights(cFuelWeights: cGraphData.fuelWeights),
                                   fuelVectors: unmarshalFuelVectors(cFuelVectors: cGraphData.fuelVectors),
                                   fuelBurnIsOutOfEnvelope: cGraphData.fuelBurnIsOutOfEnvelope,
                                   curtailedEnvelopes: [WBStructs.Envelope](),
                                   error: WBStructs.Error())
     }
    

    /*
     May not be needed any more
     
    //Converts C FuelVectors struct to a Swift FuelVectors struct
    static func unmarshalFuelVectors(cFuelVector: FuelVectors) -> WBStructs.FuelVectors
    {
        return WBStructs.FuelVectors(enroute: unmarshalFuelVector(cFuelVector: cFuelVector.enroute),
                                     ballast: unmarshalFuelVector(cFuelVector: cFuelVector.ballast))
    }
 */
}
