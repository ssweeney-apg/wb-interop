//
//  wb-marshalling.swift
//  wb-framework
//
//  Created by Victor Graham on 10/7/20.
//  Copyright © 2020 Aircraft Performance Group. All rights reserved.
//

//https://www.raywenderlich.com/7181017-unsafe-swift-using-pointers-and-interacting-with-c


import Foundation
import WBLibrary

//Class used to convert Swift weight and balance data structures to weight and balance C library data structures
class WBMarshal
{
    //Converts a Swift Tail struct to a C Tail struct.
    //This dynamically allocates memory for various members of the C struct, but not the Tail struct itself.
    //Memory must be deallocated. Function freeTail is designed to deallocate the memory
    //that was allocated here.
    static func marshalTail(swiftTail: WBStructs.Tail) -> Tail
    {
         var cTail = Tail(basicEmpty: Mass(weight: swiftTail.basicEmpty.weight,
                                           moment: (swiftTail.basicEmpty.moment[Axis.x.rawValue],
                                                    swiftTail.basicEmpty.moment[Axis.y.rawValue],
                                                    swiftTail.basicEmpty.moment[Axis.z.rawValue])),
                          cabinItems: marshalCabinItems(swiftCabinItems: swiftTail.cabinItems),
                          cabinItemsCount: swiftTail.cabinItems.count,
                          mac: MeanAerodynamicChord(length: swiftTail.mac.length,
                                   leading_edge: swiftTail.mac.leadingEdge),
                          weight_limits: WeightLimits(maximum_ramp_weight: swiftTail.weightLimits.maximumRampWeight,
                                                      maximum_takeoff_weight: swiftTail.weightLimits.maximumTakeoffWeight,
                                                      maximum_landing_weight: swiftTail.weightLimits.maximumLandingWeight,
                                                      maximum_zero_fuel_weight: swiftTail.weightLimits.maximumZeroFuelWeight,
                                                      minimum_weight: swiftTail.weightLimits.minimumWeight),
                          tailEnvelopes: marshalTailEnvelopes(swiftTailEnvelopes: swiftTail.envelopes),
                          fuel_tank_data: marshalTankData(swiftTankData: swiftTail.fuelTankData),
                          taxiBurnTankIndex: swiftTail.taxiBurnTankIndex,
                          fuelDensity: swiftTail.fuelDensity,
                          fuel_tank_count: swiftTail.fuelTankData.count,
                          fuelLoads: marshalFuelLoads(swiftFuelLoads: swiftTail.fuelLoads),
                          fuelLoadCount: swiftTail.fuelLoads.count,
                          burn_rules: marshalBurnRules(swiftBurnRules: swiftTail.burnRules),
                          burn_rule_count: swiftTail.burnRules.count,
                          curtailment: marshalCurtailments(swiftCurtailment: swiftTail.curtailments),
                          zeroFuelEnvelopeOnly: swiftTail.zeroFuelEnvelopeOnly,
                          max_fuel_point_count: 0);
        
        cTail.max_fuel_point_count = GetTailMaxFuelPointCount(&cTail);
        
        return cTail;
    }
    
    
    //Frees memory dynamically allocated to member of a C Tail struct.
    //Intended to work with marshalTail.
    static func freeTail(cTail: Tail)
    {
        freeTailEnvelopes(cTailEnvelopes: cTail.tailEnvelopes)
        freeTankData(cTankData: cTail.fuel_tank_data, dataCount: Int(cTail.fuel_tank_count));
        freeBurnRules(cBurnRules: cTail.burn_rules, ruleCount: Int(cTail.burn_rule_count));
        freeFuelLoads(cFuelLoads: cTail.fuelLoads, fuelLoadCount: Int(cTail.fuelLoadCount))
        freeCabinItems(cCabinItems: cTail.cabinItems)
    }
    

    //Converts an array of Swift Mass structs to an array of C Mass structs.
    //This dynamically allocates memory for the C array and returns a pointer.
    //Memory must be deallocated, but it's a single command so a freeMasses function is not provided.
    //cMass.deallocate();
    static func marshalMasses(swiftMasses: [WBStructs.Mass]) -> UnsafeMutablePointer<Mass>
    {
        let cMasses = UnsafeMutablePointer<Mass>.allocate(capacity: swiftMasses.count);
        
        for i in 0..<swiftMasses.count
        {
            cMasses[i] = Mass(weight: swiftMasses[i].weight,
                           moment: (swiftMasses[i].moment[Axis.x.rawValue],
                                    swiftMasses[i].moment[Axis.y.rawValue],
                                    swiftMasses[i].moment[Axis.z.rawValue]));
        }
        
        return cMasses;
    }
    
    
    //Converts an array of Swift LoadRule structs to an array of C LoadRule structs.
    //This dynamically allocates memory for the C array and returns a pointer.
    //Memory must be deallocated. Function freeLoadRule is designed to deallocate the memory
    //that was allocated here.
    static func marshalLoadRules(swiftLoadRules: [WBStructs.LoadRule]) -> UnsafeMutablePointer<LoadRule>
    {
        let cLoadRules = UnsafeMutablePointer<LoadRule>.allocate(capacity: swiftLoadRules.count)
        
        for i in 0..<swiftLoadRules.count
        {
            cLoadRules[i] = LoadRule(sequence: swiftLoadRules[i].sequence,
                                     fuelTankID: swiftLoadRules[i].fuelTankID,
                                     loadVolume: (swiftLoadRules[i].loadVolume),
                                     fuelRatios: UnsafeMutablePointer<FuelLoadRatio>.allocate(capacity: (swiftLoadRules[i].fuelRatios.count)),
                                     fuelRatioCount: swiftLoadRules[i].fuelRatios.count,
                                     untilFullTankID: swiftLoadRules[i].untilFullTankID,
                                     loadCondition: LoadConditions(UInt32(swiftLoadRules[i].condition.rawValue)))
            
            //Fuel ratio array
            for j in 0..<swiftLoadRules[i].fuelRatios.count
            {
                cLoadRules[i].fuelRatios[j].fuelTankID = swiftLoadRules[i].fuelRatios[j].fuelTankID
                cLoadRules[i].fuelRatios[j].ratioVolume = swiftLoadRules[i].fuelRatios[j].ratioVolume
            }
        }
        
        return cLoadRules
    }

    
    //Frees memory dynamically allocated to an array of C LoadRule structs and memory dynamically allocated
    //to each load rule's FuelRatio array.
    //Intended to work with marshalLoadRules.
    static func freeLoadRules(cLoadRules: UnsafeMutablePointer<LoadRule>, ruleCount: Int)
    {
        //First free memory allocated to fuel ratio arrays
        for i in 0..<ruleCount
        {
            cLoadRules[i].fuelRatios.deallocate();
        }
        
        //Free memory allocated to burn rules array
        cLoadRules.deallocate();
    }
    
    
    //Converts an array of Swift FuelLoad structs to an array of C FuelLoad structs.
    //This dynamically allocates memory for the C array and returns a pointer.
    //Memory must be deallocated. Function freeFuelLoads is designed to deallocate the memory
    //that was allocated here.
    static func marshalFuelLoads(swiftFuelLoads: [WBStructs.FuelLoad])-> UnsafeMutablePointer<FuelLoad>
    {
        let cFuelLoads = UnsafeMutablePointer<FuelLoad>.allocate(capacity: swiftFuelLoads.count)
        
        for i in 0..<swiftFuelLoads.count
        {
            cFuelLoads[i] = FuelLoad(loadRules: marshalLoadRules(swiftLoadRules: swiftFuelLoads[i].loadRules),
                                     loadRuleCount: swiftFuelLoads[i].loadRules.count,
                                     maxFuelLoadGallons: swiftFuelLoads[i].maxFuelLoadGallons,
                                     minFuelLoadGallons: swiftFuelLoads[i].minFuelLoadGallons,
                                     weightHigh: swiftFuelLoads[i].weightHigh,
                                     weightLow: swiftFuelLoads[i].weightLow)
        }
        
        return cFuelLoads
    }

    
    //Frees memory dynamically allocated to an array of C FuelLoad structs and memory dynamically allocated
    //to each FuelLoad's LoadRules array.
    //Intended to work with marshalFuelLoads.
    static func freeFuelLoads(cFuelLoads: UnsafeMutablePointer<FuelLoad>, fuelLoadCount: Int)
    {
        //First free memory allocated to fuel ratio arrays
        for i in 0..<fuelLoadCount
        {
            freeLoadRules(cLoadRules: cFuelLoads[i].loadRules, ruleCount: Int(cFuelLoads[i].loadRuleCount))
        }
        
        //Free memory allocated to burn rules array
        cFuelLoads.deallocate()
    }

    
    //Converts an array of Swift BurnRule structs to an array of C BurnRule structs.
    //This dynamically allocates memory for the C array and returns a pointer.
    //Memory must be deallocated. Function freeBurnRules is designed to deallocate the memory
    //that was allocated here.
    static func marshalBurnRules(swiftBurnRules: [WBStructs.BurnRule]) -> UnsafeMutablePointer<BurnRule>
    {
        let cBurnRules = UnsafeMutablePointer<BurnRule>.allocate(capacity: swiftBurnRules.count);
        
        for i in 0..<swiftBurnRules.count
        {
            cBurnRules[i] = BurnRule(fuel_tank_id: swiftBurnRules[i].fuelTankID,
                                     destination_fuel_tank_id: swiftBurnRules[i].destinationFuelTankID,
                                     weight: swiftBurnRules[i].weight,
                                     sequence: swiftBurnRules[i].sequence,
                                     type: BurnTypes(UInt32(swiftBurnRules[i].type.rawValue)),
                                     condition: BurnConditions(UInt32(swiftBurnRules[i].condition.rawValue)),
                                     condition_fuel_tank_id: swiftBurnRules[i].conditionalFuelTankID,
                                     condition_value: swiftBurnRules[i].conditionValue,
                                     fuel_ratios: UnsafeMutablePointer<FuelBurnRatio>.allocate(capacity: swiftBurnRules[i].fuelRatios.count),
                                     fuel_ratio_count: swiftBurnRules[i].fuelRatios.count,
                                     hard_code_burn_type: HardCodeBurnTypes(UInt32(swiftBurnRules[i].hardCodeBurnType.rawValue)));
            
            //Fuel ratio array
            for j in 0..<swiftBurnRules[i].fuelRatios.count
            {
                cBurnRules[i].fuel_ratios[j].fuel_tank_id = swiftBurnRules[i].fuelRatios[j].fuelTankID;
                cBurnRules[i].fuel_ratios[j].ratio_volume = swiftBurnRules[i].fuelRatios[j].ratioVolume;
            }
        }
        
        return cBurnRules;
    }
    
    //Frees memory dynamically allocated to an array of C BurnRule structs and memory dynamically allocated
    //to each burn rule's FuelRatio array.
    //Intended to work with marshalBurnRules.
    static func freeBurnRules(cBurnRules: UnsafeMutablePointer<BurnRule>, ruleCount: Int)
    {
        //First free memory allocated to fuel ratio arrays
        for i in 0..<ruleCount
        {
            cBurnRules[i].fuel_ratios.deallocate();
        }
        
        //Free memory allocated to burn rules array
        cBurnRules.deallocate();
    }
    
    
    //Converts an array of Swift TankData structs to an array of C TankData structs.
    //Volume data is included, but capacity data and mass data is not.  SetTankArrayValues must be called to initialize this data.
    //This dynamically allocates memory for the C array and returns a pointer.
    //Memory must be deallocated. Function freeTankData is designed to deallocate the memory
    //that was allocated here.
    static func marshalTankData(swiftTankData: [WBStructs.TankData]) -> UnsafeMutablePointer<TankData>
    {
        let cTankData = UnsafeMutablePointer<TankData>.allocate(capacity: swiftTankData.count);
        
        for i in 0..<swiftTankData.count
        {
            cTankData[i] = TankData(tank_id: swiftTankData[i].ID,
                                    density: swiftTankData[i].density,
                                    point_count: swiftTankData[i].volumePoints.count,
                                    volume_points: UnsafeMutablePointer<Volume>.allocate(capacity: swiftTankData[i].volumePoints.count),
                                    mass_points: UnsafeMutablePointer<Mass>.allocate(capacity: swiftTankData[i].volumePoints.count),
                                    volume_capacity: 0,
                                    weight_capacity: 0);
            
            //Tank volume data array
            for j in 0..<swiftTankData[i].volumePoints.count
            {
                cTankData[i].volume_points[j] = Volume(volume: swiftTankData[i].volumePoints[j].volume,
                                                       cg: (swiftTankData[i].volumePoints[j].cg[Axis.x.rawValue],
                                                            swiftTankData[i].volumePoints[j].cg[Axis.y.rawValue],
                                                            swiftTankData[i].volumePoints[j].cg[Axis.z.rawValue]));
            }
        }
        
        SetTankArrayValues(cTankData, swiftTankData.count);

        return cTankData;
    }
    
    //Frees memory dynamically allocated to an array of C TankData structs and memory dynamically allocated
    //to each tank's volume and mass points arrays.
    //Intended to work with marshalTankData.
    static func freeTankData(cTankData: UnsafeMutablePointer<TankData>, dataCount: Int)
    {
        //First free memory allocated to fuel ratio arrays
        for i in 0..<dataCount
        {
            cTankData[i].volume_points.deallocate();
            cTankData[i].mass_points.deallocate();
        }
        
        //Free memory allocated to burn rules array
        cTankData.deallocate();
    }
    
    
    //Converts a Swift Envelope struct to a C Envelope struct.
    //This dynamically allocates memory for the C limit points arrays.
    //Memory must be deallocated. Function freeEnvelope is designed to deallocate the memory
    //that was allocated here.
    //envelope.minimum_moment_limit_points.deallocate();
    //envelope.maximum_moment_limit_points.deallocate();
    static func marshalEnvelope(swiftEnvelope: WBStructs.Envelope) -> Envelope
    {
        return Envelope(minimum_weight: swiftEnvelope.minimumWeight,
                        maximum_weight: swiftEnvelope.maximumWeight,
                        minimum_moment_limit_points: marshalMasses(swiftMasses: swiftEnvelope.minimumCGLimitPoints),
                        minimum_moment_limit_point_count: swiftEnvelope.minimumCGLimitPoints.count,
                        maximum_moment_limit_points: marshalMasses(swiftMasses: swiftEnvelope.maximumCGLimitPoints),
                        maximum_moment_limit_point_count: swiftEnvelope.maximumCGLimitPoints.count)
    }

    
    //Converts a Swift DesignEnvelope struct to a C DesignEnvelope struct.
    //This causes memory to be dynamically allocated for the C limit points arrays.
    //Memory must be deallocated. Function freeDesignEnvelopes is designed to deallocate the memory
    //that was allocated here.
    static func marshalStandardEnvelopes(swiftStandardEnvelopes: WBStructs.StandardEnvelopes) -> DesignEnvelopes
    {
        return DesignEnvelopes(inflight: marshalEnvelope(swiftEnvelope: swiftStandardEnvelopes.inflight),
                               takeoff: marshalEnvelope(swiftEnvelope: swiftStandardEnvelopes.takeoff),
                               landing: marshalEnvelope(swiftEnvelope: swiftStandardEnvelopes.landing),
                               zeroFuel: marshalEnvelope(swiftEnvelope: swiftStandardEnvelopes.zeroFuel))
    }
    
    
    //Frees memory dynamically allocated to an array of C Envelope structs and memory dynamically allocated
    //to each envelope's minimum moment limit and maximum moment limit arrays.
    //Intended to work with marshalEnvelopes.
    static func freeDesignEnvelopes(cDesignEnvelopes: DesignEnvelopes)
    {
        //Free memory allocated to minimum and maximum moment limit arrays
        cDesignEnvelopes.inflight.minimum_moment_limit_points.deallocate();
        cDesignEnvelopes.inflight.maximum_moment_limit_points.deallocate();
        
        cDesignEnvelopes.takeoff.minimum_moment_limit_points.deallocate();
        cDesignEnvelopes.takeoff.maximum_moment_limit_points.deallocate();
        
        cDesignEnvelopes.landing.minimum_moment_limit_points.deallocate();
        cDesignEnvelopes.landing.maximum_moment_limit_points.deallocate();
        
        cDesignEnvelopes.zeroFuel.minimum_moment_limit_points.deallocate();
        cDesignEnvelopes.zeroFuel.maximum_moment_limit_points.deallocate();
    }
    
    
    //Converts a Swift TailEnvelopes struct to a C TailEnvelopes struct.
    //This dynamically allocates memory for the C fixed curtailment limit points arrays for all envelopes.
    //Memory must be deallocated. Function freeTailEnvelopes is designed to deallocate the memory
    //that was allocated here.
    static func marshalTailEnvelopes(swiftTailEnvelopes: WBStructs.TailEnvelopes) -> TailEnvelopes
    {
        return TailEnvelopes(design: marshalStandardEnvelopes(swiftStandardEnvelopes: swiftTailEnvelopes.design),
                             fixedCutailment: marshalEnvelope(swiftEnvelope: swiftTailEnvelopes.fixedCurtailment))
    }
 
    //Frees memory dynamically allocated to an array of C CabinItem structs.
    //Intended to work with marshalCabinItems.
    static func freeTailEnvelopes(cTailEnvelopes: TailEnvelopes)
    {
        freeDesignEnvelopes(cDesignEnvelopes: cTailEnvelopes.design)
        
        cTailEnvelopes.fixedCutailment.minimum_moment_limit_points.deallocate()
        cTailEnvelopes.fixedCutailment.maximum_moment_limit_points.deallocate()
    }
    

    //Converts an array of Swift cabin item structs to an array of c cabin item structs
    //This dynamically allocates memory for the C array and returns a pointer.
    //Memory must be deallocated. Function freeCabinItems is designed to deallocate the memory
    //that was allocated here.
    static func marshalCabinItems(swiftCabinItems: [WBStructs.CabinItem]) -> UnsafeMutablePointer<CabinItem>
    {
        let cCabinItems = UnsafeMutablePointer<CabinItem>.allocate(capacity: swiftCabinItems.count)
        
        var i = 0
         for swiftCabinItem in swiftCabinItems
        {
            cCabinItems[i] = CabinItem(arm: swiftCabinItem.arm,
                                       cabinItemID: swiftCabinItem.cabinItemID,
                                       centroid: swiftCabinItem.centroid,
                                       filled: swiftCabinItem.filled,
                                       maxCount: swiftCabinItem.maxCount,
                                       maxWeight: swiftCabinItem.maxWeight,
                                       referenceCabinItemId: swiftCabinItem.referenceCabinItemID,
                                       seatCount: swiftCabinItem.seatCount,
                                       subType:
                                        {
                                            () -> CabinItemSubTypes in
                                            
                                            switch swiftCabinItem.subType
                                            {
                                            case "PAX":
                                                return subTypePax
                                                
                                            case "CABIN":
                                                return subTypeCabin
                                                
                                            case "CARGO":
                                                return subTypeCargo
                                                
                                            case "CARGOD":
                                                return subTypeCargoD
                                                
                                            case "PILOT":
                                                return subTypePilot
                                                
                                            case "ACITEM":
                                                return subTypeACItem
                                                
                                            case "CREW":
                                                return subTypeCrew
                                                
                                            case "LOCATION":
                                                return subTypeLocation
                                                
                                            default:
                                                return subTypeUnknown
                                            }
                                        }(),
                                       weight: swiftCabinItem.weight)
            
            i += 1
        }
        
        return cCabinItems
    }

    
    //Frees memory dynamically allocated to an array of C CabinItem structs.
    //Intended to work with marshalCabinItems.
    static func freeCabinItems(cCabinItems: UnsafeMutablePointer<CabinItem>)
    {
        cCabinItems.deallocate()
    }
    
    
    //Converts a Swift Double array to a C double array.
    //This dynamically allocates memory for the C array and returns a pointer.
    //Memory must be deallocated, but it's a single command so a freeDoubles function is not provided.
    //cDouble.deallocate()
    static func marshalDoubleArray(swiftDoubles: [Double]) -> UnsafeMutablePointer<Double>
    {
        let cDoubles = UnsafeMutablePointer<Double>.allocate(capacity: swiftDoubles.count)
        
        var i = 0
 
        for swiftDouble in swiftDoubles
        {
            cDoubles[i] = swiftDouble
            i += 1
        }

        return cDoubles
    }
    
    
    //Converts a Swift BurnWeight struct to C BurnWeight struct.
    static func marshalBurnWeight(swiftBurnWeight: WBStructs.BurnWeight) -> BurnWeight
    {
        return BurnWeight(fuelTankID: swiftBurnWeight.fuelTankID,
                          weight: swiftBurnWeight.weight)
    }

    //Converts a Swift CustomBurnProfile struct to C CustomBurnProfile struct.
    //Memory must be deallocated. Function freeCustomBurnProfile is designed to deallocate the memory
    //that was allocated here.
    static func marshalCustomBurnProfile(swiftCustomBurnProfile: WBStructs.CustomBurnProfile) -> CustomBurnProfile
    {
        let cCustomBurnProfile = CustomBurnProfile(taxiFuelWeight: swiftCustomBurnProfile.taxiFuelWeight,
                                                   taxiBurnTankIndex: swiftCustomBurnProfile.taxiBurnTankIndex,
                                                   burnWeights: UnsafeMutablePointer<BurnWeight>.allocate(capacity: swiftCustomBurnProfile.burnWeights.count),
                                                   burnWeightsCount: swiftCustomBurnProfile.burnWeights.count)
        
        var i = 0
        
        for burnWeight in swiftCustomBurnProfile.burnWeights
        {
            cCustomBurnProfile.burnWeights[i] = marshalBurnWeight(swiftBurnWeight: burnWeight)
            i += 1
        }
        
        return cCustomBurnProfile
    }
    
    //Frees memory dynamically allocated to the BurnWeights array in a C CustomBurnProfile struct.
    //Intended to work with marshalCabinItems.
    static func freeCustomBurnProfile(cCustomBurnProfile: CustomBurnProfile)
    {
        cCustomBurnProfile.burnWeights.deallocate()
    }
    
    //Converts a Swift CurtailmentSettings struct to C CurtailmentSettings struct.
    static func marshalCurtailmentSettings(swiftCurtailmentSettings: WBStructs.CurtailmentSettings) -> CurtailmentSettings
    {
        return CurtailmentSettings(useCurtailmentEnvelope: swiftCurtailmentSettings.useCurtailmentEnvelope)
    }
    
    //Converts a Swift Curtailment struct to C Curtailment struct.
    static func marshalCurtailments(swiftCurtailment: WBStructs.Curtailments) -> Curtailments
    {
        return Curtailments(settings: marshalCurtailmentSettings(swiftCurtailmentSettings: swiftCurtailment.settings))
    }
}
